package ru.pisarev.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class EmptyPasswordHashException extends AbstractException {

    @NotNull
    public EmptyPasswordHashException() {
        super("Error. Password hash is empty");
    }

}
