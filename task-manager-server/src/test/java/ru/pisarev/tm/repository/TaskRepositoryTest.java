package ru.pisarev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.pisarev.tm.model.Task;

import java.util.List;

public class TaskRepositoryTest {

    @Nullable
    private TaskRepository taskRepository;

    @Nullable
    private Task task;

    @Before
    public void before() {
        taskRepository = new TaskRepository();
        task = taskRepository.add("testUser", new Task("Task"));
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @NotNull final Task taskById = taskRepository.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task, taskById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskRepository.findAll("testUser");
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Task> tasks = taskRepository.findAll("test");
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    public void findById() {
        @NotNull final Task task = taskRepository.findById("testUser", this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Task task = taskRepository.findById("testUser", "34");
        Assert.assertNull(task);
    }

    @Test
    public void findByIdNull() {
        @NotNull final Task task = taskRepository.findById("testUser", null);
        Assert.assertNull(task);
    }

    @Test
    public void findByIdIncorrectUser() {
        @NotNull final Task task = taskRepository.findById("test", this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void remove() {
        taskRepository.removeById(task.getId());
        Assert.assertNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertNull(taskRepository.removeById(null));
    }

    @Test
    public void findByName() {
        @NotNull final Task task = taskRepository.findByName("testUser", "Task");
        Assert.assertNotNull(task);
    }

    @Test
    public void findByNameIncorrect() {
        @NotNull final Task task = taskRepository.findByName("testUser", "34");
        Assert.assertNull(task);
    }

    @Test
    public void findByNameNull() {
        @NotNull final Task task = taskRepository.findByName("testUser", null);
        Assert.assertNull(task);
    }

    @Test
    public void findByNameIncorrectUser() {
        @NotNull final Task task = taskRepository.findByName("test", this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    public void findByIndex() {
        @NotNull final Task task = taskRepository.findByIndex("testUser", 0);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeById() {
        taskRepository.removeById("testUser", task.getId());
        Assert.assertNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void removeByIdNull() {
        Assert.assertNull(taskRepository.removeById("testUser", null));
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final Task task = taskRepository.removeById("testUser", "34");
        Assert.assertNull(task);
    }

    @Test
    public void removeByIdIncorrectUser() {
        @NotNull final Task task = taskRepository.removeById("test", this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void removeByIndex() {
        @NotNull final Task task = taskRepository.removeByIndex("testUser", 0);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeByName() {
        @NotNull final Task task = taskRepository.removeByName("testUser", "Task");
        Assert.assertNotNull(task);
    }

    @Test
    public void removeByNameIncorrect() {
        @NotNull final Task task = taskRepository.removeByName("testUser", "34");
        Assert.assertNull(task);
    }

    @Test
    public void removeByNameNull() {
        @NotNull final Task task = taskRepository.removeByName("testUser", null);
        Assert.assertNull(task);
    }

    @Test
    public void removeByNameIncorrectUser() {
        @NotNull final Task task = taskRepository.removeByName("test", this.task.getName());
        Assert.assertNull(task);
    }

}