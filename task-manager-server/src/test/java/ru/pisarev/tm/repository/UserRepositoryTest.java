package ru.pisarev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.pisarev.tm.model.User;

import java.util.List;

public class UserRepositoryTest {

    @Nullable
    private UserRepository userRepository;

    @Nullable
    private User user;

    @Before
    public void before() {
        userRepository = new UserRepository();
        @NotNull final User user = new User();
        user.setLogin("User");
        user.setEmail("email");
        this.user = userRepository.add(user);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("User", user.getLogin());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("email", user.getEmail());

        @NotNull final User userById = userRepository.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user, userById);
    }

    @Test
    public void findAll() {
        @NotNull final List<User> users = userRepository.findAll();
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findById() {
        @NotNull final User user = userRepository.findById(this.user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final User user = userRepository.findById("34");
        Assert.assertNull(user);
    }

    @Test
    public void findByIdNull() {
        @NotNull final User user = userRepository.findById(null);
        Assert.assertNull(user);
    }

    @Test
    public void findByLogin() {
        @NotNull final User user = userRepository.findByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByLoginIncorrect() {
        @NotNull final User user = userRepository.findByLogin("34");
        Assert.assertNull(user);
    }

    @Test
    public void findByLoginNull() {
        @NotNull final User user = userRepository.findByLogin(null);
        Assert.assertNull(user);
    }

    @Test
    public void findByEmail() {
        @NotNull final User user = userRepository.findByEmail(this.user.getEmail());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByEmailIncorrect() {
        @NotNull final User user = userRepository.findByEmail("34");
        Assert.assertNull(user);
    }

    @Test
    public void findByEmailNull() {
        @NotNull final User user = userRepository.findByEmail(null);
        Assert.assertNull(user);
    }

    @Test
    public void remove() {
        userRepository.removeById(user.getId());
        Assert.assertNull(userRepository.findById(user.getId()));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertNull(userRepository.removeById(null));
    }

    @Test
    public void removeById() {
        userRepository.removeById(user.getId());
        Assert.assertNull(userRepository.findById(user.getId()));
    }

    @Test
    public void removeByIdNull() {
        Assert.assertNull(userRepository.removeById(null));
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final User user = userRepository.removeById("34");
        Assert.assertNull(user);
    }

    @Test
    public void removeUserByLogin() {
        @NotNull final User user = userRepository.removeUserByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

    @Test
    public void removeUserByLoginIncorrect() {
        @NotNull final User user = userRepository.removeUserByLogin("34");
        Assert.assertNull(user);
    }

    @Test
    public void removeUserByLoginNull() {
        @NotNull final User user = userRepository.removeUserByLogin(null);
        Assert.assertNull(user);
    }

}