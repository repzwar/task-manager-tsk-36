package ru.pisarev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.endpoint.*;

public interface ServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    DataEndpoint getDataEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @Nullable
    Session getSession();

    void setSession(Session session);
}
