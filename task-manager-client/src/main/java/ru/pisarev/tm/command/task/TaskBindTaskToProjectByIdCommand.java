package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.endpoint.Task;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.util.TerminalUtil;

import static ru.pisarev.tm.util.TerminalUtil.incorrectValue;

public class TaskBindTaskToProjectByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-bind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to project.";
    }

    @Override
    public void execute() {
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(getSession(), taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final Task taskUpdated = serviceLocator.getTaskEndpoint().bindTaskById(getSession(), taskId, projectId);
        if (taskUpdated == null) incorrectValue();
    }
}
