package ru.pisarev.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.ISessionRepository;
import ru.pisarev.tm.model.Session;

import java.util.List;
import java.util.stream.Collectors;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    @Nullable
    public List<Session> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        return entities.values().stream()
                .filter(o -> userId.equals(o.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public void removeByUserId(@Nullable final String userId) {
        if (userId == null) return;
        @Nullable List<Session> sessions = findAllByUserId(userId);
        if (sessions == null) return;
        sessions.forEach(o -> entities.remove(o.getId()));
    }

}
